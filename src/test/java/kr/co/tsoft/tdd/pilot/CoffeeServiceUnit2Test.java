package kr.co.tsoft.tdd.pilot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import kr.co.tsoft.tdd.pilot.model.Coffee;
import kr.co.tsoft.tdd.pilot.service.CoffeeService;
import kr.co.tsoft.tdd.pilot.service.SimpleCoffeeRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CoffeeServiceUnit2Test {

    private SimpleCoffeeRepository simpleCoffeeRepository;
    private CoffeeService coffeeService;

    @BeforeEach
    public void setUp() {
        simpleCoffeeRepository = new SimpleCoffeeRepository();
        Coffee coffee = new Coffee("mocha");
        log.debug("coffee={}", coffee);
        simpleCoffeeRepository.add(coffee);
        coffeeService = new CoffeeService(simpleCoffeeRepository);
    }

    @Test
    public void findByName() {
        Coffee coffee = coffeeService.findByName("mocha");
        assertEquals("mocha", coffee.getName());
    }
}