package kr.co.tsoft.tdd.pilot.service;

import org.springframework.stereotype.Service;

import kr.co.tsoft.tdd.pilot.dto.CoffeeRepository;
import kr.co.tsoft.tdd.pilot.model.Coffee;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class CoffeeService {

    // @Autowired
    private CoffeeRepository coffeeRepository;

    @Builder
    public CoffeeService(CoffeeRepository coffeeRepository) {
        this.coffeeRepository = coffeeRepository;
    }

    public Coffee findByName(String name) {
        return coffeeRepository.findByName(name);
    }
}