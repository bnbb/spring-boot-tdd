package kr.co.tsoft.tdd.pilot.service;

import java.util.HashMap;
import java.util.Map;

import kr.co.tsoft.tdd.pilot.dto.CoffeeRepository;
import kr.co.tsoft.tdd.pilot.model.Coffee;

/**
 * SimpleCoffeeRepository
 */
public class SimpleCoffeeRepository implements CoffeeRepository {

    private final Map<String, Coffee> coffeeMap = new HashMap<>();

    @Override
    public Coffee findByName(final String name) {

        return coffeeMap.get(name);
    }

    @Override
    public void add(final Coffee coffee) {
        coffeeMap.put(coffee.getName(), coffee);

    }
}