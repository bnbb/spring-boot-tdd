package kr.co.tsoft.tdd.pilot.model;

import java.util.UUID;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@NoArgsConstructor
@ToString
public class Coffee {
    private String id;
    private String name;

    @Builder
    public Coffee(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }
}