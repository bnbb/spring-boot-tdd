package kr.co.tsoft.tdd.pilot.dto;

import kr.co.tsoft.tdd.pilot.model.Coffee;

public interface CoffeeRepository {
    Coffee findByName(String name);

    void add(Coffee coffee);
}